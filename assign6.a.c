#include <stdio.h>

void pattern(int p); //function to control the pattern
void Row(int q); //function to print each line of the pattern
int NoOfRows=1; //initial value for number of rows
int R; //variable to get the input

void pattern(int p)
{

    if(p>0)
    {
        Row(NoOfRows);
        printf("\n");
        NoOfRows++;
        pattern(p-1);

    }
}


void Row(int q)
{
    if(q>0)
    {
        printf("%d",q);
        Row(q-1);
    }
}

int main()
{

    printf("Please enter the number of Rows : ");
    scanf("%d",&R);
    pattern(R);
    return 0;
}
